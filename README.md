# Blankcanvas

Blankcanvas is a robust, non-opiniated workflow that allows maximum connectivity between users and data integration meaning more things stay in sync and brings all aspects of a team together for full collabrative experience and notification.

### Getting setup

1. Install [python](https://www.python.org/downloads/) which is easy on Windows and Mac but for linux install type the following command:

```bash
sudo apt-get install python
```

1. Install yarn you will need to add `sudo` for admin on Linux and Mac, for windows run CMD in administrator mode. To do this you will need NodeJS:

```bash
npm i -g yarn
```

2. Clone the Blankcanvas git repo:

```bash
git clone https://gitlab.com/Modcat/blankcanvas
```

3. Installing dependencies (run each line separately) and where you see `<project directory>` replace with the path to the cloned repo:

```bash
cd <project directory>/desktop
yarn
cd ../mobile
yarn
```

### Getting docs setup

Setting up the local docs we need docsify heres the setup:

```bash
npm i -g docsify
cd <project directory>/docs
docsify serve docs
```