# OCSI - Open Creative Software Initiative

The open creative software initiative will allow ideas to be open and free. You may freely take from these ideas and contact us if you have any you would like to share openly. The main reason for this is to collaborate with top creative software vendors and allow more creativity to enhance all creative software across the world without having to pay heavy software patents.

#### Terms & Conditions

This document and all it’s content including any embedded Youtube content is licensed under the GPL 3.0 license.

You may not use any of these ideas in patents and will not prevent any other creative software vendor from creating the same idea in any language or creative software package and all derivative works herein.

The original ideas document can be tracked on the Blankcanvas repository and can provide proof of previous ideas and when those ideas where released.

WE DO NOT in anyway say these ideas are certified or safe to use but ideas are released as soon as possible, any use of these ideas in software is AT YOUR OWN RISK and we provide NO WARRANTY or RESPONSIBILITY of any legal action whatsoever.

## Youtute Ideas

##### Open graphics idea

**Published 6 Dec 2018**
This sets out the basic template model for Blankcanvas and all multi type apps in general. This is the root of the idea and explains a lot of concepts behind connecting all environments together.

[](https://www.youtube.com/embed/9y77Rucfwrc ':include :type=iframe width=640 height=480 frameborder=0 allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen')

##### Software ideas for the open community

**Published 8 Dec 2018**
This video explains more ideas on the environments, artboards and contents and how the different environments work with the user generated artboards.

[](https://www.youtube.com/embed/1Dvm3PSaGI4 ':include :type=iframe width=640 height=480 frameborder=0 allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen')

##### VR / AR Design

**Published 9 Dec 2018**
Blankcanvas will work in all environments and scale over the network to allow as much access as possible whilst still being feasable. In this case we can share Blankcanvas over a network and connect using a browser, this allows us to share the interface and interact with our document on consoles, other computers, tablets and even AR and VR head / handsets allowing for testing content on artboards and allowing them to produce more integration with creativity.

[](https://www.youtube.com/embed/hYGoQo-1Rv8 ':include :type=iframe width=640 height=480 frameborder=0 allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen')

##### Responsive Artboards

**Published 9 Dec 2018**
This video explains the user generated artboards ability to scale to different sizes providing flexibility to published documents but also flexibility within the application itself.

[](https://www.youtube.com/embed/5BDPT23KGEQ ':include :type=iframe width=640 height=480 frameborder=0 allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen')

##### Full explaniation

**Published 9 Dec 2018**
All these ideas are now coming to a form, but we must define at this stage the foundation of an all in one app that integrates multiple environments and brings them closer together.

[](https://www.youtube.com/embed/dAGzL4_FJms ':include :type=iframe width=640 height=480 frameborder=0 allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen')

##### New ideas

**Published 10 Dec 2018**
New ideas will include things like app integration and how our app will integrate into other environments.

[](https://www.youtube.com/embed/wQhOll-LeU0 ':include :type=iframe width=640 height=480 frameborder=0 allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen')

##### Color UI

**Published 13 Dec 2018**
The color UI is more important to the average designer and also how to integrate a very sleek and nice interface when applying colors is essential for the look and feel of any graphical assets. Blankcanvas will make this easier but ultimately allow flexibility to choose an appropriate color pallete.

[](https://www.youtube.com/embed/FggSbHtgVJs ':include :type=iframe width=640 height=480 frameborder=0 allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen')

##### Architecture

**Published 16 Dec 2018**
This video will demonstrate some of the frameworks that will be used to build Blankcanvas and also how we will structure the main state of the application. This will also demonstrate how we modify and update the application state including flat mapping.

[](https://www.youtube.com/embed/2yi7VXdC6JE ':include :type=iframe width=640 height=480 frameborder=0 allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen')

##### VR & AR

**Published 17 Dec 2018**
Here we look directly at how artboards and the Blankcanvas user interface will automatically work with VR and AR and web experience platforms to closely tie in all environments and make them accessible to multiple platforms.

[](https://www.youtube.com/embed/1ix-VC1trOQ ':include :type=iframe width=640 height=480 frameborder=0 allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen')

##### Database Space

**Published 19 Dec 2018**
Blankcanvas will want a full integration with datasets such as spreadsheets but also databases. Providing more creativity in the database including useful enhancement for physical server farms but also more visual for basic setups and information.

[](https://www.youtube.com/embed/Z2WNPpKeYOo ':include :type=iframe width=640 height=480 frameborder=0 allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen')

##### Social Media Integration

**Published 19 Dec 2018**
Blankcanvas will allow storage and retreival of assets from social media platforms allowing you to access assets from anywhere and allow a more personal touch to creativity if you wish.

[](https://www.youtube.com/embed/tpG8Tf1lSWo ':include :type=iframe width=640 height=480 frameborder=0 allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen')

##### Creative Audio

**Published 20 Dec 2018**
Audio has been a central part of our creativity but now we can provide more creativity by closely coupling creative environments into the overall application of Blankcanvas.

[](https://www.youtube.com/embed/ghl6BHGbEZQ ':include :type=iframe width=640 height=480 frameborder=0 allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen')

##### Artboards Connectivity

**Published 22 Dec 2018**
In Blankcanvas you can now embed artboards into other artboards, using them in anyway and represent any environment. For example an artboard could represent a data set from a spreadsheet or database.

[](https://www.youtube.com/embed/Ct_acGO-3SU ':include :type=iframe width=640 height=480 frameborder=0 allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen')

##### Director Mode

**Published 23 Dec 2018**
Director mode allows aucistration of devices to put on show so to speak and allow for multiple devices to sync their actions at timed events. Making event planning a lot easier from a technical perspective.

[](https://www.youtube.com/embed/vBQrIbjZXCo ':include :type=iframe width=640 height=480 frameborder=0 allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen')

##### New Audio Mapping

**Published 24 Dec 2018**
Some people can make a beat with their mouth or actions but cannot translate that into musical notes. Blankcanvas will help solve that by using recored audio and transplate the pitch, beat pattern and tone into an instrutment of choice and make that easily editable.

[](https://www.youtube.com/embed/4MBytDlwnQM ':include :type=iframe width=640 height=480 frameborder=0 allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen')

##### Improved Audio

**Published 24 Dec 2018**
Improved audio visuals that can map audios volume, pitch and even a source such as a microphone to a video and filter for ultimate effects.

[](https://www.youtube.com/embed/zPfKAormuy0 ':include :type=iframe width=640 height=480 frameborder=0 allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen')

##### New Ideas

**Published 30 Dec 2018**
More clarity of how the interface works and better understand of the ideas we have so far.

[](https://www.youtube.com/embed/GUc37zBkxx4 ':include :type=iframe width=640 height=480 frameborder=0 allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen')

##### Spreadsheet

**Published 8 Jan 2019**
Manipulate spreedsheat data like a database using SQL and display data to an artboard in a visual way.

[](https://www.youtube.com/embed/ZkTBPc2sRmM ':include :type=iframe width=640 height=480 frameborder=0 allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen')

##### CMS Integration

**Published 20 Jan 2019**
We now explore using Vue, Angular or React components in a creative environment.

[](https://www.youtube.com/embed/YWY4u4ZJ8K0 ':include :type=iframe width=640 height=480 frameborder=0 allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen')

##### Notes

**Published 21 Jan 2019**
We can now attach notes to all environments, sequence the notes as if to step through them and have them integrated into the AR or VR space.

[](https://www.youtube.com/embed/d-z0QQFL0p0 ':include :type=iframe width=640 height=480 frameborder=0 allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen')

##### CI Integration

**Published 6 May 2019**
Continious integration will allow a workflow when publishing to create a pipeline for data to travel and even creative works between the environments. Now documents are based around a git repository we can easily execute external pipelines as well.

[](https://www.youtube.com/embed/RTVS6e3Rh-I ':include :type=iframe width=640 height=480 frameborder=0 allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen')

##### Air Guesture

**Published 6 May 2019**
Air gesture allows control over how the interface works on a whole host of devices with a camera attached and some user motions.

[](https://www.youtube.com/embed/NpisO5J4Jtw ':include :type=iframe width=640 height=480 frameborder=0 allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen')

##### Database Integration

**Published 6 May 2019**
1. Flat map data
2. Upload object with a memory address
3. Store in a database or scratch database
4. Share with all broadcasted sockets
5. Update the content on the user's app, or the devices mobile browser

[](https://www.youtube.com/embed/mLWqPLqUG4I ':include :type=iframe width=640 height=480 frameborder=0 allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen')

##### Live Mockup in Blankcanvas

**Published 21 May 2019**
Imagine creating live mockups and allowing them to be interactive at every level from video, AR, VR, social media, spreadsheet, graphs, database and more in a live mockup. Interacting with the device from touch or mouse and keyboard. Everything live and a new way to display your art.

[](https://www.youtube.com/embed/8kZH8QJBvO8 ':include :type=iframe width=640 height=480 frameborder=0 allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen')

##### IPO Curve Animations

**Published 22 Apr 2020**
Using Blender's UI we will use IPO curves to manipulate animations and they timing of those animations.

[](https://www.youtube.com/embed/nuOOSaJyLMM ':include :type=iframe width=640 height=480 frameborder=0 allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen')

##### Blankcanvas Document Sharing and Live Shopping

**Published 22 Apr 2020**

[](https://www.youtube.com/embed/HuFAbsEhOzY ':include :type=iframe width=640 height=480 frameborder=0 allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen')

##### 3D Canvas Fluid Simulations

**Published 3 May 2020**

[](https://www.youtube.com/embed/TCbQMs1i6i8 ':include :type=iframe width=640 height=480 frameborder=0 allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen')

##### Postman UI for Blankcanvas

**Published 3 May 2020**

[](https://www.youtube.com/embed/ZMAcwY-sCYo ':include :type=iframe width=640 height=480 frameborder=0 allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen')

##### Screen Recording

**Published 3 May 2020**

[](https://www.youtube.com/embed/H3ZFVni57kA ':include :type=iframe width=640 height=480 frameborder=0 allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen')

##### Live Assets / Notes / Git Project Management and more

**Published 7 May 2020**

[](https://www.youtube.com/embed/H3ZFVni57kA ':include :type=iframe width=640 height=480 frameborder=0 allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen')

##### Live Assets / Notes / Git Project Management and more

**Published 7 May 2020**

[](https://www.youtube.com/embed/O_yUecUtUmw ':include :type=iframe width=640 height=480 frameborder=0 allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen')

##### Blankcanvas More Ideas

**Published 8 May 2020**

[](https://www.youtube.com/embed/5O9Mw8IxAE0 ':include :type=iframe width=640 height=480 frameborder=0 allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen')


## Ideas Document

This document has and always has been under the GNU GPL 3.0. Any attempt to patent or thus otherwise break the license agreement will be considered a legal matter and a breach herein of its use. This document may NOT BE INFRINGED BY PATENTS OR PROPRIETARY SOFTWARE OR ITS VENDOR. Any implementation of the ideas will rest with the software vendor to prove they can used these ideas and take formal responsibility for all ideas implemented and will not hold Blankcanvas or Conceivable Media responsible for use of these ideas:
      
### Before 27/11/2019
**Most if not all of these ideas are covered in the Youtube videos and have greater clarification.**

- Easy armature detection AK / AF / masking / animation Global and document notes
- 3D scenes embedded and IPO curves for animation (keyframes on timeline)
- Split screen presentation where one annotates (pre-narrated slides )
- Google docs / zero layer integration (special presentation layer and other elements are converted to normal layers)
- Encrypted documents open in the browser
- Virtual shopping with each other through socket connections providing an encrypted link possibly password
- Fluid simulation of paints on a 3D rotatable canvas
- In code view show relative documentation of differing frameworks and individual pieces of frameworks, also a search feature as well
- In code view allow projects to have pre-defined XHR request's for graphQL and standard HTTP requests
- Live screen previews, see all connected devices and what they are doing (this will need user's permission explicitly upon request)
- Animated notes for annotations on design, graphics, 3D environment and a series of notes when needed, summary or small notes can open up big notes
- Synthesizer / instruments / vocal tuning interface can be shared over the network to a tablet or mobile device
- Artboards can be embedded into other artboards not just differing documents
- Git sync between contributors live preview, preview sync
- Git allows branches for a single artboards or set of artboards and syncs back to prevent conflict resolution and a pull request to project manager
- Layers can isolate a few 
- Quick search / quick look / speech search on mobile devices with a preview of the canvas access and it will then focus on that element
- Streamed asset class for example a video live streaming into asset class like a camera, drone or other footage
- Passing notes or messages back and forth to the user, including sending to the users phone
- Embedding audio projects into video timeline from another project and will update the live assets upon save
- Audio assets can be live as well
- Tone matching between two or more assets for easier composites and a slider for variable tolerance
- Masks can have filters on apply to mask or be clipped by the mask
- Masks can apply to objects but also to filters, so only certain areas will filters be applicable
- VR processing off device using a video stream for on device viewing (VR headset data is transmitted)
- Differing tool behaviour based on the user's peripherals­
- Emulation on device mock-ups, attach events and allow live previews of data and data manipulation
    - Attach events to assets on artboard and works in live mock-up / with live mock-up
    - Story walking feature in mock-ups as well, define a story and then allow each step to be iterated with notes, animation even animated annotations
- Marketing suite that works with alliance with Blankcanvas assessing analytical data determining the success of a project and then working with that failures as well, also an algorithm for success or failure
- Camera or screen capture live feed, as an output device, with manipulation of clips, code, artwork, audio, assets, live mockups, audio effects and visual effects. This will be an output feed acting like a web cam even in live feed and can be recorded upon user request. This makes streaming more interesting and is available to all applications on the device including live conferencing app's by selecting blankcanvas camera.
- Audio filters that cover tracks and are none destructive in nature over a segment in the audio file. For example these filters can be applied to multiple tracks on the timeline between the starting and ending time frames.
- Audio based filters work by using the sound waves to adjust the filters settings
- Mapping 3D or 2D vector to armature structure, using constraints we can assign assets or parts of assets to a structure and then animate the structure which will allow us to animate the assets with multiple states
- Synthesizer that can repeat multiple audio patterns on device
- Audio mapping to selected instruments, for example you can select certain drums and a symbol. Then the software will map to best possible sound from the selected synthesized instruments
- Blankcanvas code can be in separate branches under a primary user, many managers find it difficult to work with user's or other coders but now our software lets different coders communicate between lead and sub developer under git branches. A branch lets us discover and review code before a pull request do the manager can provide feedback.
- Audio synthesizer that can be shared across the network in which blankcanvas is running or blankcanvas cloud application. It will have a divided grid, each section attains either to an instrument or effect. Everything is no destructive and all patterns are replaceable with differing instruments or filters. You can also lock cords or set a defined amount of cords that can be mapped through touch or audio input. It will allow selecting of different instruments using the same pattern and remapping tone to match the instrument.
- Add graph data from a spreadsheet or database and have it placed within a 3D scene or live mockup and when the data is changed it automatically updates the data

### 27/11/2019

Mapping out an environment using AR tech, stream video from Oculus and then map the environment out for real-time game play mapping. Combining AR with VR
- Create real-time texture mapping for already landscaped places, programmatic texture, spawn of enemies in the mapped environment / automatic AI
- Create deep scenes against walls making the environment seem  much larger
- Texture based database for dynamic textures that can be loaded from a feathers real-time database
- Allow maskable areas to allow real view into 3D render textures, for safety and other cool effects
- Map out the game being played real-time within a separate AR environment
- Sharing the main document settings when trying to connect another Blankcanvas server that provides a document
- Act out motion sensing to map armature structure and then map to that structure
- Server reviews all actions and keeps track of all the game and the player scores and information
- Console can run Blankcanvas giving all game access, then someone on a gamepad can review the game and or build, change and adapt the environment added new objects and interactive elements at realtime
- Allow masking of transparent planes like windows make it look like a real window and people / enemies can come from outside
- Luminosity detection which changes the light and colour tonal matching of 3D environments making it more realistic
- Remapping gun positions for handheld devices
- Autonomous background checks
- Calculator for cheaper hosting (seller by store)
- Main blank canvas login for cloud based documents
- Streaming of emulators to Blankcanvas
- Notes included on the presentation
- Live stream presentations to all devices (responsive if applicable)
- Code integrates with CODEPEN and also MDN quick search
- SSH / FTP access client build in (server available on the dartboards)
- Automatic file syncing on components on art board
- Virtual shared drive
- Software real time language translation
- Easy global matting algorithm, selection of faces (face detection) / solid objects in a scene including in video
- Easy face detection in video

### 15/07/2020

- A video timeline will in a segment or the entire timeline, allow different versions of the timeline data. As if taking a snapshot of the clips in their position, meta data, position, filters, rotation, clipping and any associated data. The user can then present differing versions of the data by selecting between each snap shop.
- The video editor will allow grouping of layers or tracks making them collapsible and editable together. This can then have filters and masks applied to all tracks in the group, also the masks and filters are animatable with the IPO curve editor.
- Video filters can be applied in part or in whole over the video or graphical content. These filters can be animation not just positionally but also the values of filters can be modified over time using interpolation points or key frames with tweening and easing.
- Audio track filters can be animated, all options of the filter can be animated and manipulated over time and re-editable. Also on top of the IPO curve editor adjusting filters over time using interpolation points or key frames with tweening and easing. The filters can also be modified over interpolation of touch devices like tablets and smart phones in a completely no destructive way.

### 22/07/2020

- A timeline with layers or tracks and have clips placed on them. Blankcanvas will allow a line to be drawn vertically and edited either to be diagonal or a bezier curve which the selected clips on the timeline will anchor to the curve or line from the top left and then an offset parameter will allow this anchor to change in percentage and gravity making the staggering effect greater or smaller depending on the curve and the offset. Also this curve can also effect the position of animations or tweens again via the same principle.

### 24/07/2020

- Store history as JSON string and then parse when the user selects that history object.
- Interface can be divided just like blender as panels can be divided and merged on their adjacent row or column. It can even apply to separate windows. This then can be stored as JSON and the interface can be restored or saved in different workspaces such as video, graphics, database and more. Allow the user to fully define the interface.
- Filters can have masks applied directly where the mask controls the size, flow and dimensions of where the filter is applied to the video or image. These masks can also be animated on the timeline, including properties of the masks like position, size and morph into different shapes using key frames. Masks can be hierarchical meaning a parent mask can apply to several filters and then each filter can it’s own mask respecting the parent. Also bleeding is allowed for example the parents edges can be feathered with a child’s blurred mask.
- Global matting or other algorithms applied to an image can have a mask. For example only part of an image that is masked can use facial detection. This limitation of the algorithm will speed up how it is applied, also we can add animation to the mask again position, scale and morphing so that during a video or animation the algorithm can only be applied to a certain area or time.

### 27/07/2020

- User can pin a terminal or series of terminals to a tab in code mode. This means when changed focus of the code editor the terminals will also change to what the user has assigned.
- Mapping of face to characters even in video
- Mapping of openCV and masking areas for greater performance
- Running global matting algorithms onto video and masking it
- Object tracking and descriptions. Software analyses the objects and defines them as they are. User selects object and a mask can be generated and also follow content on video. Then openCV or filters can be applied to detected objects. All filters and openCV are none destructive.
- All of the following about object tracking, openCV and filters can be included in live streams on cameras that transmit over a network or any streamed content that can be buffered over the internet.
- Full openCV on all motion detection including input detection. A user can supply images or symbols their looking for like road signs. Also motion detect hand gestures and tracking motion that can be applied to 3D modes and even particle simulations within the 3D.

### 04/08/2020

- Groups can have masks and effects applied in a video timeline that are no destructive.
- Masks on groups or layers can be applied over the entire clip, or just part of the timeline. Everything about the mask or filter can be adjusted such as size, shape and the prominence of effects can vary according to the IPO graph.
- Groups can have no destructive tweening, this can be applied to all the clips or even audio within the group and have all of their effects and prominence tweaked over time.
- Masks can be linked together so you can create a mask with a name, then you can apply it for a period on the timeline. Then you can clone the mask, retain all the details and animation of the mask and then re-apply to another copy. This link can be broken and then simply a clone will be deduced from this effect.
- Groups and layers can now be in the interface of audio as well. Also groups can have the same no destructive audio effects applied to all or part of the group.

### 06/08/2020 Gaming engine:

- Take any physical map and then turn it into SVG paths for the roads and landscape high and low terrane. Using the high and low you can easily create realistic and probable mountains and clefs. 
- Using climate data you can have the terrane mapped with certain weather, seasons and style of terrane.
- Terrane can change upon seasons, the texture and color matches the season.
- Terrane and textures can be painted, as well as objects such as trees, buildings and animal boundaries.
- Every object can have pre baked breakable points.
- Every object has methods or actions that the user interacts with in the game, all these methods can be mapped to a key, console button and area which the user enters into.
- All objects have the ability with each action placed on that object allow the character to be animated in a certain way.
- Code canvases / webgl is allowed in the video editor but also allows live streamed content audio or video to be placed on the timeline and have effects applied in real time or not. The code can define parameters or options that can be defined and control in a user interface using controls defined in the video editor. The video editor will automatically render the controls for the parameters set by the programmer and adjusted by the user in a none destructive manner.

### 07/08/2020

- Workspace much like blender’s will be implemented with panels split vertical and horizontal. You can also merge vertical and horizontal. Panels can be opened in new windows. Panels can be grouped under tabs in one pane. For example you can merge several panel types in one pane using tabs and an icon that represents that panel type.
- Global properties such as, database, spreadsheet and other data. Each panel for database, spreadsheet or any other data driven panes work independently from one another. Some panels are closely coupled, in this case they will work with that type of pane for example database or spreadsheet. When that panel is focused on it updates the other panes that are classed as relational into that type. Relational panes only work with the focused data, it has to be in the global store to do this.

### 08/08/2020

- Spreadsheets and databases live embeddable in notes and see live connections in the notes or database.
- Live charts embeddable in notes and when data changes in spreadsheet or database that charts will update.
- Notes can have spreadsheets, data or live mockups embedded into them including any artboard. This ensures easy editing and will also carry the permissions of who can edit the data, video, artboard or anything presented in a note that is embedded from any of the environments available in the blankcanvas software.
- Blankcanvas docs are loosely coupled to attached files.
- Blankcanvas scans the network, to see who is accessing shared assets on a local drive. If so public information will be provided over the network by default so that all devices on that network will automatically and cohesively work together across the local network.
- Blankcanvas cloud will bridge the gap between the users cloud credentials and other users. The cloud will manage permissions between local and external users only granting access when the permissions are granted.
- Gitlab has open source technology that allows the infrastructure to be copied by to blankcanvas servers. Also it can be hosted locally and embed into Blankcanvas itself treating each open file as a repository with full logs and information. This will especially be useful for data driven or code projects are involved and even extend to video and graphical editing.
    - Gitlab will embedding the Blankcanvas app and server
    - Once a .bcd doc is opened it will be treated as a repo, with fill CI / CO integration without leaving the currently environment.
    - Attached files on a local system is fine, if the files is moved a copy of the data will be used from the last saved copy. Also a saved copy of the data which is moved will try to be track using the OS’s search features. However the program will recommend cloud hosting either their own or blankcavas and keep copies and synced copies.

### 13/08/2020

- Video editor can add assets onto the canvas that are code based
- Video, graphical or any editor can share a sub canvas or asset to a screen on another device. That device could be a tablet, desktop, laptop or any IOT connected device and have live feedback.
- Videos can be embedded into notes including different versions of the video, with the timeline embedded. It can be the entire project or snippet of the video.
- Any part of the graphical interface can be embedded in the notes with their editing interface as an optional extra. When clicking the asset which could be graphical, video, audio, spreadsheet, database, artflow or director it open that project environment with the selected asset.
- Data from databases, spreadsheets or any other source can be presented in notes in many different formats including raw or graphical like a chart. When clicked one can open the source of the data in the respected environment with the dataset open.
- Graphical interfaces will be able to utilize all technologies available using WASM or web assembly, python, c++, c, c#, javascript and any other applicable language that doesn’t require the user to apply additional installations.
- The main graphical interface will implement HAXE and export to HASHLANG which can run natively on any platform with the HASHLANG VM. The user will be able to code with the assets they create in the digital interface by ID’s and classes. Also the graphical interface will create code and the user may manipulate that code based on input and state of the asset.
- Any graphical interface may use physics and simulation in any environment It may export that environment’s data (parameters) to create that simulation over a network, over a network to another app or just to another app like blender to complete the simulation and return the results rendered or not.
- Audio data may be processed off device for example a table and be rendered on the desktop for sound effects and visuals.
- Audio can have live assets or recordings streamed into the app.
- Blankcanvas will allow quick and easy application extension with live code even in the running desktop environment. The plugin will be isolated for this reason, no network access unless explicitly specified and whitelisted devices can only connect. The dev extension tool will be made available to all users in dev mode to help develop their plugin. Hot reloading will be applied running the environment off a server rather than static files.
- It will create a simple boiler plate and provide documentation for the plugin
- It will open dev tools and allows the plugin to follow strict but easy to follow code design patterns
- Allows easy interface design by object structure and data binding
- Allows easy unit test creation for their classes and applies those test automatically into a graphical Gitbook or similar technology for the test to viewed by the developer and others.
- Director mode will allow data to be sent to a group of devices, servers or endpoints in realtime as show or timeline goes on. Any data or information can be shared to the consumer through push notifications, ecommerce applications or any app that the user currently is connected to. For example a singer comes on stage it can push a notification and populate an app with all the clothing and items associated with that entertainer that is on stage. This could be applied to any event and situation you could think of.
- Director mode will allow others to communicate and geolocate on another upon permissions being granted in an event including find other people, business and stalls of interest. It may also provide suggestions on previously accessed stalls, talks and information that have attended around the event.
- Director mode may direct information as the user walks past a certain part of the event using geolocation. For example they get to the front of the queuing area and it presents a notification to display their ticket information automatically. Also it may present information if they go near a talk stand or stage.
- Director mode timeline can be divided into segments. On the Y axis you have layers and groups stacked. On the X axis you have segments that are divided up by different sections that can be collapsed, hidden or deleted. This is good for the event planning as it will allow segments of the event to be sliced out. For example a segment could represent an artist, band or act and if they fail to make the event you can cut that segment out and live edit even as the event is currently playing out.
- Any graphical artboard or set of artboard / documents should be exportable to any device that allows the HASHLANG VM.
- Blankcanvas will allow downloading of the document skeleton but will only download the parts of the repo that matter to the user and download on demand the parts that it needs. It will automatically download linked documents as well.
- Blankcanvas using the gitlab repo will allow a histories panel that spans the history of that file and changes in any environment even local databases, graphical, video, audio or any other content.
    - Editor interface can be attached to embedded artboards or documents. For example even video from a video project can have a timeline represented graphically and editable in another artboard where it is embedded. Live mockups!
    - Auto help will updated when Blankcanvas changes environment and also when in code it will change depending on the framework you are using. Including feathers, vue or any other framework native and none native including suggested snippets course that are free and commercially available through the store or online.
    - Tester window, this will allow you to send pre defined HTTP requests and even a series or hierarchy of requests to preform. This will allow you to save a set of requests, in sequence or randomly for certain directories or projects that continually allow for testing. It will also produce an output file that uses JS to run such requests in a embedded interface to run directly in the browser.
    - When Blankcanvas changes environment it will remember the workspace in it’s last state. However will also remember external windows that have their workspace open and only open those windows on that environment and close when exiting the environment when the pop out window does not apply to that environment.
    - Automatic compiling of images, where assets, images or vectors, will be assessed by OpenCV and a detailed list of objects, people or scenery in those images will be presented to the user. The user will select which parts of each image they wish to compile. The outline of OpenCV will be passed to the global matting image algorithm for it be carefully cut out depending on the level of quality the user has inputted. Then the user will select one of the assets for the colour and blending definition or they can create a colour pallet to match or another image they wish to use specifically for colour manipulation. Once complete all assets will be compiled into one image with each asset on a new layer and colour matched. This is none destructive so you can easily change the object and color mode in which that asset exists, including the entire compiled image.

### 14/08/2020

- Github comparison works by a simple tree structure. To resolve code issues in code it will blur the interface and show the two conflicts allowing a simple two buttons between saying previous or newer version. Git conflicts on a graphical plane it will allow a split view, you will be able to look at the old version and new version side by side on a graphical canvas. The divider line will be moveable allow you to directly overlay the new and old and mask to see the difference then a simple two buttons on each side of the divider will say accept this version. Also the header will be new and old. Also
- Director mode will allow collision detection on motion based devices. In director mode there will be 3D paths and pre runs only in software that can detect collisions before they happen. This collision will be based on two 3D object bounding box and safety zones defined by the device or user. If they collide in the simulator then it will alert the user and not allow the devices on those motion paths to work. Don’t forget motion paths can have multiple devices attached to them, it will detect any collision on any device, a motion path consists of X, Y and Z axis so it will be able to detect altitude as well.
- Director mode will be able to detect safety zones where people will be in the event, ensuring that any fly over device will keep a safe distance from the people.
- Director mode will be able to map network output if no instructions are given for the device by the manufacturer. For example if a drone has no API or documented API then Blankcanvas will monitor your network and ask you to follow some instructions, much like a dance mat that asks you to follow instructions left, right, up or down and look at the packed data in order to copy and run in simulator as well as communicate with the device through the mapped packets to produce the flight path and make the device work with those mapped instructions. Those instructions can be edited on the code editor as well.
- Director mode will allow streaming from other devices directly to another device. For example you may be able to live stream drone footage, being recorded live, to a screen that is connected to director. Also you can stream live streams that can be edited in real time or apply real time visual and audio effects.
- Director mode will work with social media, whether it be hash tags, live streams, shared streams, live posts and feeds or previously posted. Allowing the audience to interact with the devices in director for example displaying tweets in a live stream on a TV or it could ask questions to the user for example it may ask the users which flight path for the drone to take with different options. Director mode will allow this feedback and work with this system to create more interactivity.
- Audio interface has an open API that any manufacturer can implement and allow sound engineers to pre configure speakers, microphones or any audio equipment and save pre settings  using the devices UUID or similar identifiers allow engineers to setup wirelessly or weird their devices quickly with pre baked files and collections of files for re-usable setups.

### 17/08/2020

- Above an artboard there will be a notes icon, toggling this button will hide or show notes attached to the relevant artboard.
- The render process will be multi threaded each artboard is rendered on a thread. Video artboards can be multithreaded again for rendering and exporting purposes.
- There will be three editable lines that can be converted and manipulated like the pen tool, there will be an X, Y and Z which can manipulate either a 3D mesh plane, warp 3D objects, particle simulation or a meshed vertex plane with particles or objects attached to it’s vertices as an emitter object.
- Layers panel will only show layers & groups for the active artboard, however double clicking an embedded artboard will load in layers and groups from that artboard
- Gaming engine has ready made skeleton's, such as moving cars, planes, motors, human body and more of all sizes. Individual components can be multiplied like wheels with suspension and track rod physics. This will allow the physics engine and the user easy creation and manipulation of 3D object typically found in the scene. Also a library can store these assets to be sold or reused at any available time.

### 19/08/2020

- Gaming engine: Skeletons can have default audio and visuals applied. For example audio for a mechanical skeleton of a car could be the audio for acceleration, de-acceleration, skidding and more. Also visuals could be default animations for a person skeleton would be walk, run, crawl and so forth and these will be mapped to default controller or keys.
- Timing and project management can be integrated into all parts of the application through the cloud. These cloud notes can be applied to timeline, IPO animator timed notes for video or interactive content also code and graphical interfaces.
- When creating new jobs within the cloud it can generate new branches for the job from the chosen original selected branch and timed operations will apply. Full push notifications will apply when jobs moved into the designated project management section of the Kanban board.

### 20/08/2020

- Presentation: Embed all mediums of data, including mockups with live mockups of other artboards. Embed spreadsheet or database in an editor or environment format that can be exported with the document. Also in the form of animated graphical form as well with endpoint data and live URL and socket information if not found or the database connection fallback data is used from last exported date. Also embeddable web views are possible with simple mockups.
- Presentation: Embedded emulators that can run in the presentation viewport, this include full emulation of the device not just a live stream of the device. Live code viewing as well with hot reloading in the presentation and easy steps through the code and can even change the code to different versions of the code updating the embedded emulator and also allows live coding as well.
- Presentation: Embed prototyped interface with the ability to change annotations and notes on the slide as you navigate the prototype in a single presentation slide. It will appear as a flow chart and notes and annotations can be attached at each stage with the default notes and annotations will attached to the first slide.
- Project management: you can embed git projects inside of other git projects. Importing jobs and any associated data. This means we can have Blankcanvas files that link multiple git repos together without being connected.
- Project management: Users can encrypt all, sections such as groups of artboards, video, spreadsheets, databases or any group or a single file contained within a Blankcanvas document. This ensures an extra layer of security inside the Blankcanvas document and full ACL (Access Control Levels) on groups, single files or the whole document ensuring permissions are set on a general or finite level.

### 24/08/2020

- Documents: All documents can have an embedded editable environment on export straight in the browser with all the connection details. This may require a username or password. All documents can be encrypted in part or all and even environments such as editable and previewing. Also they can limit the exported interface to only allowable tools even basing this on individual users which is enforced on the client side and server side.
- Library can work differently in different environments, the library can change from code snippets, SQL statements, graphical elements depending on the group or environment the user is in.
- Database: save SQL queries globally or custom for each type of database, also access to c# language for statements to be run against any type of database like PDO in PHP.
- Library can run images through openCV and do object detection, colour swatches and more. This enables the library to do complex calculations and image selection very quickly.
- Database: the database environment can detect common CMS types and suggest download or reusing some SQL statements.
- Presentation can be viewable on an array of mockup designs, so they can view how their presentation will look on a mobile phone, VR headset (using a streamed camera output if one is provided a default backdrop if not) or even a desktop or laptop computer.
- Presentation: Presentations can function differently on different devices. Artboards or slide are responsive but how they behave in a presentation can be different as well depending on the device. So for example how things are animated or the transitions and styles can change. The way this is done is through break points, these allow the artboard to be responsive and to have different animations or clear previous animations for different break points.
- Code: the code environment can be embedded in an exported workspace. This allow’s the user to edit the workspace without installing Blankcanvas. Also it will allow the users to test their code in web assembly code right in the browser without having complications of compilation. Also if we need compilation we can use connection information embedded in the workspace to connect with a server and compile the code sent over a network without the need for compilation. Also we can send the code over the network which can run in a pre defined docker image or receive the docker image from the client. Also the online website or exported version may also call a locally installed version of docker or Kubernetes if it is present and the user decides so.
- Presentation: import artflow, user stories and annotations into presentation mode. This will allow users to modularly define a user story and then play that story out in presentor mode. Also presentations will allow you to select many user stories and have that automatically presented in a presentation slide show.
- Presentation: A presentation can account for the device it’s running on or as a side note. If the user is watching a presentation the presentation may at a trigger point by selecting a user story, interactive quiz or any other call to action will allow live mockups on devices such as a walk through of the interface by actually clicking hotspots, it may be a fully functional interface itself running in an emulated environment or simulator and actually run a code project with an actual fully functional interface. It will also allow popups with information and embedded media such as videos or audio as well to walk the user throught the user stories journey as they view the presentation at the same time. This can run realtime on the users device or it can be used as a side note for example the main presentor on his desktop shows the user slideshow on the main projector, all attending the meeting may have a device mobile, tablet or desktop connected to the presentation allowing live notes to passed to them. This can the launch the application or mockup application to the user’s device and allow that user to use their device to walk through stories on their device in real time. You can even cancel the emulation or mockup when the master slide moves on or allow the users to continue with playing around on the user’s custom device. You can also provide different emulators or mockups to different environments so for example the emulator maybe nativescript on mobile or electron for desktop. You can run custom scripts, docker environments or emulation scripts that may run on a user’s device. The artboard its being responsive can choose it’s emulator at differing breakpoints.
- Presentation: You can apply animations just is presentational mode only, allowing you to make sure artboard animations stay separate from presentational animations.
- Presentation: You can attach an artboard animation to a presentation trigger allowing the user to reuse the artboard’s embedded animation and control.
- Artflow: User stories change depending on the device specific needs. For example one story could be a “user buys an item” the flow of this story maybe different to that of one on a desktop to a mobile. So to cater for this a story will have different modes for desktop, tablet, mobile and any other mode the user wishes to define. That way you can one user story with many different versions for different devices.
- Artflow: User stories may change depending on region or language or any custom attribute. Sometimes user stories can be adapted based on region, language or even a custom attribute the user defines like subscriptions, different subscription levels may alter or modify the user story.
- Artboard: An artboard can have different modes such as wireframe and full fidelity design. The user can define custom modes for the artboard, the artboard will keep the same size and dimension when any mode changes the size all modes change with it. Overlays can also be applied for example a wireframe artboard can be mirrored onto a full fidelity artboard with transparency and place above or blow on the full fidelity mode. This allows the user to modify each envitonment and import it into any other environment as a layer that can be moved and adjusted but not editable by any other environment.

### 24/08/2020

- Published documents viewable in devices may connect to a remote server to allow for annotations or comments and will show up live in the Blankcanvas interface and vice versa.
- Artboards can be linked together like pages on a word document or pages for brochure in one artboard interface.
- Artboards can have modes set, so an artboard can change the look and feel of the interface depending on what the user’s currently active selected artboard. For example a user clicks on a document type artboard this will change the Blankcanvas workspace to suit a document environment like LibreOffice writer. Then when the user clicks on an artboard in a graphical mode the interface will change the Blankcanvas workspace to suit a more graphical interface it will change the layout of panels and even the panels themselves to suit the user. The user is able to define new artboard modes in Blankcanvas settings and customise things from there.
- The interface panels (small building blocks that make up the workspace) can change their behaviour, layout and control flow depending on what the user selects and hide options not applicable to the selected items.

### 28/10/2020

- Videos stories, embed stories as video content in artflow, even presentational aspects in artflow this can attached to screens or displayed next to or on the screen that has the video tutorial about that screen.
- Have presentor (slideshow presentation) export to artflow for a story for example and vice veras
- UI libraries mapping values like rebass (use less), in other words artboards can share a brand guidelines artboard / config file and this can then be shared across the artboards, visual aspects and web components. The config file itself can be exported to rebass and other UI libraries across all different frameworks.
- Blankcanvas will use SSL cert's or any encryption with webRTC to avoid heavy server load
- Blankcanvas video / narrator audio recording to walk through artflow not just in the artflow but they complement the product as an embedded artboard in artflow
- presentations can be interactive for the time the slide is available, for example an interactive mobile app people can click, touch and interact as the slide is being presented once off the slide it is no longer presented even displaying live apps or websites.

### 29/11/2020

- Blankcanvas videos stories, embed stories as video content in artflow, even presentational aspects in artflow
- Have presentor export to artflow to for a story for example
- UI libraries mapping values like rebass (use less)
- SSL cert's via email with webRTC
- Blankcanvas: video / narrator audio recording to walk through artflow not just in the artflow but they complement the product
- Theme file should be generic
- Presentations can be interactive for the time the slide is available, for example an interactive mobile app people can click, touch and interact as the slide is being presented
- Graphics allows you close integration with UI frameworks such as Rebass, tailwind and many another’s. You can also define rules that will check spacing, padding, margin and typography anything you want and through a linting process will show any errors and mismatches between design and the design contract.
- webRTC connection through an SPA allows for the browser to render the Blankcanvas SPA and then communicate through webRTC cutting costs down and eleminating server load. All logs modifications will be fed back to a host computer or to a github repo.
- Graphics when repeatable components from a shared source (a component that will be attached to many different documents) these components can have constraints of what can be editable and what can't these can change but they will change across all instances in every document. This allows a strict design contract so components can maintain form and allow a subset of properties to change.
- Graphics components can have real GraphQL or rest queries supplied to them, or dummy queries you can execute. In some cases data varies so you can also have these queries attached to components and run that list of queries on the component. 
- Graphics components can be demoed with different data being passed to show all states that component's can live in.
- Graphics theme contracts will apply styling in less (CSS preprocessor) so you can have a code first approach to design contracts.

### 02/12/2020

- Blankcanvas notes can be exported out into docsify allow a clear and simple instruction and each notebook and note can be exported out in MD format and publishable to a server.
- Blankcanvas you can export permissions and even have permissions live on a server so that when working with teams your permissions will be set for each team member.
- Blankcanvas pipelines can work in any environment such as database, code and spreadsheet. The pipline can run whenever there is an update / merge request made on the repo. Ensuring that when data it is updated can then be reflected for example lets say we need to convert spreadsheet data into JSON. We would create a new pipeline that when we update our spreadsheet it will be converted into JSON and copied to the relevant place updating our graphics or presentation for example. We can code this pipeline for advanced use with command line access or simple tooling for basic usage. You would first create a pipeline and then attach it to a document or multiple documents, it is worth noting pipelines will be typed to their document type.
- Blankcanvas animation when selecting multiple elements all relevant IPO curves show on animation timeline. When selecting IPO curve it will update the view with the selected element that will be effected if they edit that IPO curve. Also the IPO panel will allow filtering of selected elements so you may only show one set of IPO curves for one element without the need for deselecting elements.
- Blankcanvas presentation when about to present call will be initiated and call can happen over webRTC. Also interactive whiteboard and chat can happen whilst presenting to allow feedback and discussion during or after presentation, this will be controlled by the presentor.
- Blankcanvas graphics, when creating a new artboard it will allow the user to select a standard size or custom size that already exists in the document. The user can still draw freehand if they want to.

### 16/12/2020

- Linked documents such as a style guide, image repository and more this will help with management of brand guidelines and integrate across all sides of the workforce from photography, print design, web design, code and any other workforce entity. Each entity can have it's own repository of files and style guides and these can be imported into other documents using an API and linked git repos.
- Publish style guides into an interface that'll allow API requests so it can integrate into other applications using taxonomies to build custom pages for export. For example you can customise the URL with taxonomies to show only print information for a brand or multiple brands.
- Code editor to allow Kubernetes integration pipeline. You would code a pipeline .yml file and then it will export / update the image for Kubernetes to then rerun the pipeline on your local device and can be mirrored to a remote source of your choosing for example a web server.

### 15/01/2021

- Director mode can be used for displaying graphics based on a segmented timeline and introduce technolog. This advances technology within director mode, as a presentation moves through the timeline we we can allow documecation with documents and ultimatelty control technology and increase learning and understanding of the conecpts. Controlling drowns. holograic technology and more features.
- Blankcanvas external files when dropping over the app it will open just that file and save just that file. If adding to a repository you can clone or have the option of running and syncing against the cloud so that it makes syncronising easier if needs be.
- Blankcanvas Audio - uplifting an application or docs state into another one for example video with all audio can be mapped out of the main video project and edited in an environment just for audio. This allows work symulatanously but will have parent constraints. This would be like the video timeline will control the positioning of audio and track location that would be mapped to the audio interface. Then both the video and audio editor can work in syncronisity or any environment and have parental constraints.
- Database manager has ability to map current database and allow for simple commits to be made upon changes and include versioning with each digram and commit phase. Allowing for the user to go back in database schema.

### 02/04/2021

- We can search artboards for components, symbols, layers or any data, the artboards matching the search criteria in and of themselves or anything placed within the artboard can be visible in the search and even highlight the found child element of the artboard for example a component embedded on the artboard will be highlighted in the search results.
- Artboards can be treated as a component, artboards can have props attached to them, we can then change a component type or an artboard acting as a component and modify the properties to change the state, look and feel of each artboard based on the props to the artboard.
- Artboards can have a type assocaited that will change the layout of the UI or add additional options such as a normal artboard will show a standard UI. However the artboard type could be changed to video, component or anything you would like. This will ensure when editing the artboard the app will open the relevant environment to edit and if a component type then you will be able to change the look and feel and state in the props and make them independant of one another.
- When artboards are component types they can conditionally render layers based on the data associated with the artboard and allow logic to be applied on the layer level to ensure that layers are visible only when needing to be.
- On the artboard layers not only are they reactive in and of themselves they can also use data for example a label on a button, we can embed data using {{ template literals }} and write expressions {{ property ? true : false }} ensuring maximum customisation and closer integration of design and developer.


### 07/02/2022

- Interface on VR devices can be pinned to the view whilst the artboard, or multiple artboards can be targeted using the VR headset.
- Each slide in a presentation, (a slide being an artboard) can allow for multiple views. On a VR headset we can view each slide on the VR headset and set 3D transitions between the slides. This will be broadcasted.
- Live updating using CI/CD pipeline without the user having to refresh the user interface is done in realtime with the network and cloud update. Event the shell of the app can update and install new depedancies and also broadcast this over the network so any locally connected devices will auto update across the network on tables, phones, consoles and VR headsets.
- The main window can act like an accessor to all the associated documents in the repo, then when clicking on an doc, graphics or anything else you can open a new window with that environments last saved workspace. If you keep the graphics environment window open and try to open another graphics file from the main window it will open up a tab and show in the document switcher in the already open window. The same goes for all types such as, doc, graphics, database and more.

### 08/02/2022

- An asset on an artboard can change it's behaviour when rendered on different devices. For example the asset could apply physics on a VR, phone or tablet device and for desktop it could simply just animate. Each asset on the artboard can have different behaviour, interactions and styles depending on the device as well as mapped interactions this will all be set through the user interface.

### 25/02/2022

- In the notes environment you can also create a mind map chart, org chart or any other chart, when clicking on a node in the chart it allows for any associated notes of that node to be viewed. For example you have a mind map and in the mind map it will say database for example, upon clicking on that node you will be able to add, edit and delete notes associated with that node. This could also apply to flow charts as well, and also the arboard environment, clicking a node on the artboard can allow associated notes with that node or element of that artboard, timeline, animation, animation keyframe.