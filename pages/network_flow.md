# Network flow

we need to know how data and connectivity will flow in Blankcanvas in a local and global environment. For now the MVP is get basic network communication but for the future a WebRTC interface with complete connectivity from start to finish.

1. Source of truth must be a git repository
2. Any editing must have access to git repository
3. Local network allows easy access for testing across multiple devices
4. We have a user management system locally and globally, they can both be managed independantly depending on the user
5. All communications must be transported safely and as secure as possible

### Basic objectives

1. WebRTC is only to keep data in sync through external connections.
    - WebRTC enabled projects must be hosted on a git repository.
    - WebRTC clients must have the same access to push and pull to the git repository.
    - We must ensure no one point of failure can occur.
2. Local network is managed locally and easily, primarily for testing and demonstrating purposes allowing easy flexible use.
3. User has control over what is viewable globally and locally and have full easy control of user management through a linked Blankcanvas cloud account.

![App Flowchart](../media/app_flow.svg "Blankcanvas application network flow")

### WebRTC flow

1. User first agree's to share a document that is cloud compatible which is a repository file living on a hosted server.
2. External users (on a completely separate network) that are linked and associated with the source account in Blankcanvas cloud can see the documents they've been granted to access to.
3. When connecting to the external document we will download the git repository onto their device (using the feathers API).
4. We then establish a WebRTC connection to the orginial device making sure the source of truth still remains the git repository.
    - This ensures we keep the goal in mind that if one Node drops the WebRTC connection that all connections are not lost and we can still work on our documents with no failure.

### Golden rules

Types of connections:

- Blankcanvas <-> external Blankcanvas (webRTC)
- Blankcanvas <-> Blankcanvas (Cloud login / request permission)
- Blankcanvas <-> Server env (Cloud login / request permission)

Golden rules for connectivity:

- Use one time session password for local authentication and device testing (same admin user on all devices).
- Use external password for external authentication, used for anyone who is not the local admin and requires permission access.

Process for external authentication on same local network:

- Make login request to Blankcanvas server by sending private and encrypted API key that is assigned to that user.
- Recieving Blankcanvas app must check that the current user will be able to identify the user by decryption and check API access token and then check they have access to the repository.
- The server will generate a one time access token for the session and send it back to the original recieving point.
- Uppon success provide a one time oauth token to login to that document.
- Blankcanvas will create a user account based on username and one time access token as password.
- Each user is identified by their unique API key and encryption, that can be revoked from a document or team at any time.
  - That user profile under that document will take the default permissions and then it can be overwritten on the document level.

### Cloud invite

When using teams on Blankcanvas cloud this will be the following process:

- Ask to invite someone to a specific team via their email (the team will need to be assigned to repos).
- Blankcanvas checks user exists and then sets up an endpoint for them to join accounts.
- Email client opens up with details and link with all the endpoint options.
- When the recipent clicks the link it is accepted and then gains access to all documents (repos) that team has access to.
  - The Blankcanvas server will grant access to the repositories.
  - Generate automatically the access token for the user on that document (note users can have different permission levels).