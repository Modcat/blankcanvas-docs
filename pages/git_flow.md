# Git flow
Git flow is to establish SSH access to a users git, then to initalise a new repository to store all of Blankcanvas's preferences.

- User open's `Cloud Login` panel
- Click setup on git
- Generate SSH key pair in local directory
- Provide user with a button to copy key to clipboard
- Provide user instructions to assign new key in Gitlab
- Test connection
- If failed or successful provide message
- Upon success, intialise the new blankcanvas repository to store preferences, notes, user data and any global information

### Linked repositories

When linking multiple documents (which are repositories) together, Blankcanvas will watch all linked repository directories and then pull down when new commits are made.

##### Dealing with conflict

Graphical, video or presentational visuals should never be distructable:

- When placing data all relevant data is copied for safety purposes and a dynamic link is created
- When dynamic data is updated so long as it's relevant and formatted correctly we will update the data
- When data is changed and IS NOT correct we keep the old data and provide user information in the interface if data is incorrectly formatted or providing the wrong data altogether

### When saving a document

When saving a document do the following:

- Commit the current changes
- Push current changes to server (we should only be working on master for now)