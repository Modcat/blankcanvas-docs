 # Licensing

Blankcanvas is dual licensed, the main documentation, framework, workflow, architecture, server, UI / UX on desktop and mobile including every aspect of the program except the renderer is under GPL 3.0. The renderer written in Haxe which displays all graphical output is licensed under Apache 2.0.

> To clarify this license is everything in Blankcanvas except the renderer

```MD
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
```

> To clarify this license applies only to the renderer

```MD
Apache Blankcanvas Renderer
Copyright 2020 The Apache Software Foundation

This product includes software developed at
The Apache Software Foundation (http://www.apache.org/).
```

To note dual licensing compatibility is very important, so to clarify we can indeed have these licenses integrated together please read compatibility docs provided by [Apache](https://www.apache.org/licenses/GPL-compatibility.html).

For anyone whom it may concern we have an [attribution page](attribution.md) to see other licenses for assets, iconography and more.